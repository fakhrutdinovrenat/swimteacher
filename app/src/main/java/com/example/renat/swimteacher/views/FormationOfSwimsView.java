package com.example.renat.swimteacher.views;

import android.support.annotation.StringRes;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

public interface FormationOfSwimsView extends MvpView {

    void setMessage(@StringRes int message);

    void slideAnimation();

    @StateStrategyType(SkipStrategy.class)
    void createSpinner();
}
