package com.example.renat.swimteacher.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.renat.swimteacher.R;

public class ProtestFragment extends Fragment {

    private Button buttonWatchForm;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View protestView = inflater.inflate(R.layout.fragment_protest, container, false);

        buttonWatchForm = protestView.findViewById(R.id.watch_form_button);
        buttonWatchForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProtestFormFragment protestFormFragment = new ProtestFormFragment();
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container, protestFormFragment).commit();
                Toast.makeText(getActivity(), R.string.protest3002, Toast.LENGTH_SHORT).show();
            }
        });

        return protestView;
    }
}
