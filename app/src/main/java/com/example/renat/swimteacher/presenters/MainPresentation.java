package com.example.renat.swimteacher.presenters;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.renat.swimteacher.views.MainView;

@InjectViewState
public class MainPresentation extends MvpPresenter<MainView> {
    public MainPresentation() {
    }
}
