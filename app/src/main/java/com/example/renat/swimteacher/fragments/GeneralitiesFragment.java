package com.example.renat.swimteacher.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.transition.Slide;
import android.transition.TransitionManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.renat.swimteacher.R;
import com.ms.square.android.expandabletextview.ExpandableTextView;


public class GeneralitiesFragment extends Fragment {

    private Spinner spinnerGeneralities;
    private ExpandableTextView expandableTextView;
    private LinearLayout llContainer, llGen1, llGen2, llGen3, llGen4;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View generalitiesView = inflater.inflate(R.layout.fragment_generalities, container, false);

        expandableTextView = generalitiesView.findViewById(R.id.expand_text_view);
        expandableTextView.setText(getString(R.string.generalities_head));

        llContainer = generalitiesView.findViewById(R.id.generalities_container);
        llGen1 = generalitiesView.findViewById(R.id.llgen1);
        llGen2 = generalitiesView.findViewById(R.id.llgen2);
        llGen3 = generalitiesView.findViewById(R.id.llgen3);
        llGen4 = generalitiesView.findViewById(R.id.llgen4);

        ArrayAdapter<CharSequence> adapterGeneralities = ArrayAdapter.createFromResource(getContext(),
                R.array.spinnerGeneralities, android.R.layout.simple_spinner_dropdown_item);
        adapterGeneralities.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerGeneralities = generalitiesView.findViewById(R.id.spinner_generalities);
        spinnerGeneralities.setAdapter(adapterGeneralities);
        spinnerGeneralities.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        TransitionManager.beginDelayedTransition(llContainer, new Slide(Gravity.START));
                        llGen1.setVisibility(View.VISIBLE);
                        llGen2.setVisibility(View.GONE);
                        llGen3.setVisibility(View.GONE);
                        llGen4.setVisibility(View.GONE);
                        break;
                    case 1:
                        TransitionManager.beginDelayedTransition(llContainer, new Slide(Gravity.START));
                        llGen1.setVisibility(View.GONE);
                        llGen2.setVisibility(View.VISIBLE);
                        llGen3.setVisibility(View.GONE);
                        llGen4.setVisibility(View.GONE);
                        break;
                    case 2:
                        TransitionManager.beginDelayedTransition(llContainer, new Slide(Gravity.START));
                        llGen1.setVisibility(View.GONE);
                        llGen2.setVisibility(View.GONE);
                        llGen3.setVisibility(View.VISIBLE);
                        llGen4.setVisibility(View.GONE);
                        break;
                    case 3:
                        TransitionManager.beginDelayedTransition(llContainer, new Slide(Gravity.START));
                        llGen1.setVisibility(View.GONE);
                        llGen2.setVisibility(View.GONE);
                        llGen3.setVisibility(View.GONE);
                        llGen4.setVisibility(View.VISIBLE);
                        break;
                }

                Toast.makeText(getActivity(), String.valueOf(parent.getItemAtPosition(position)), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        return generalitiesView;
    }
}
