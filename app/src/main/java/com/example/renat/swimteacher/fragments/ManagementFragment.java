package com.example.renat.swimteacher.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.transition.Slide;
import android.transition.TransitionManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.renat.swimteacher.R;

public class ManagementFragment extends Fragment {

    private Spinner spinnerManagement;
    private LinearLayout llContainer, llMan1, llMan2;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View managementView = inflater.inflate(R.layout.fragment_management, container, false);

        llContainer = managementView.findViewById(R.id.management_container);
        llMan1 = managementView.findViewById(R.id.llman1);
        llMan2 = managementView.findViewById(R.id.llman2);

        ArrayAdapter<CharSequence> adapterManagament = ArrayAdapter.createFromResource(getContext(),
                R.array.spinnerManagement, android.R.layout.simple_spinner_dropdown_item);
        adapterManagament.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerManagement = managementView.findViewById(R.id.spinner_management);
        spinnerManagement.setAdapter(adapterManagament);
        spinnerManagement.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        TransitionManager.beginDelayedTransition(llContainer, new Slide(Gravity.START));
                        llMan1.setVisibility(View.VISIBLE);
                        llMan2.setVisibility(View.GONE);
                        break;
                    case 1:
                        TransitionManager.beginDelayedTransition(llContainer, new Slide(Gravity.START));
                        llMan1.setVisibility(View.GONE);
                        llMan2.setVisibility(View.VISIBLE);
                        break;
                }

                Toast.makeText(getActivity(), String.valueOf(parent.getItemAtPosition(position)), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        return managementView;
    }
}
