package com.example.renat.swimteacher.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.transition.Slide;
import android.transition.TransitionManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.example.renat.swimteacher.R;
import com.example.renat.swimteacher.ResultModel;
import com.example.renat.swimteacher.firebase.Auth;
import com.example.renat.swimteacher.firebase.Data;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class TimerFragment extends Fragment {

    RelativeLayout rlContainer;
    TextView textView, textViewDialog, textViewAge;
    ImageButton btnStart, btnStop, btnLap, btnSave, btnResult;
    ToggleButton toggleSex, togglePoolSize;
    SeekBar ageSeekBar;
    Spinner spinnerType, spinnerDist;
    String toggleSexString, togglePoolSizeString, spinnerTypeString, spinnerDistString;
    long millisecondTime, startTime, timeBuff, updateTime = 0L;
    Handler handler;
    int seconds, minutes, milliSeconds;
    ListView listView;
    String[] listElements = new String[] {  };
    List<String> listElementsArrayList;
    ArrayAdapter adapter;

    private FirebaseDatabase database;
    private DatabaseReference reference;
    private FirebaseAuth mAuth;
    private FirebaseUser firebaseUser;

    public TimerFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        final View timerView = inflater.inflate(R.layout.fragment_timer, container, false);

        rlContainer = timerView.findViewById(R.id.timer_container);
        textView = timerView.findViewById(R.id.textView_timer);
        listView = timerView.findViewById(R.id.listview);
        btnStart = timerView.findViewById(R.id.btnStart);
        btnStop = timerView.findViewById(R.id.btnStop);
        btnLap = timerView.findViewById(R.id.btnLap);
        btnSave = timerView.findViewById(R.id.btnSave);
        btnResult = timerView.findViewById(R.id.btnResults);


        handler = new Handler();

        listElementsArrayList = new ArrayList<>(Arrays.asList(listElements));
        adapter = new ArrayAdapter<>(timerView.getContext(), android.R.layout.simple_list_item_1,
                listElementsArrayList);
        listView.setAdapter(adapter);

        /*mAuth = FirebaseAuth.getInstance();
        firebaseUser = mAuth.getCurrentUser();
        String userID = firebaseUser.getUid();
        database = FirebaseDatabase.getInstance();
        reference = database.getReference(userID);*/

        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Slide slide = new Slide();
                slide.setDuration(100);
                TransitionManager.beginDelayedTransition(rlContainer, slide);

                if (btnStop.getVisibility()==View.VISIBLE) {
                    startTime = SystemClock.uptimeMillis();
                    handler.postDelayed(runnable, 0);
                    btnStop.setEnabled(false);
                    btnStop.setVisibility(View.INVISIBLE);
                    btnStart.setImageResource(R.drawable.ic_pause_circle_filled_50dp);
                    btnLap.setEnabled(true);
                    btnLap.setVisibility(View.VISIBLE);
                    btnSave.setVisibility(View.INVISIBLE);
                    btnSave.setEnabled(false);
                } else {
                    timeBuff += millisecondTime;
                    handler.removeCallbacks(runnable);
                    btnStop.setEnabled(true);
                    btnStop.setVisibility(View.VISIBLE);
                    btnStart.setImageResource(R.drawable.ic_play_circle_filled_50dp);
                    btnLap.setEnabled(false);
                    btnLap.setVisibility(View.INVISIBLE);
                    btnSave.setVisibility(View.VISIBLE);
                    btnSave.setEnabled(true);
                }
            }
        });

        btnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                millisecondTime = 0L;
                startTime = 0L;
                timeBuff = 0L;
                updateTime = 0L;
                seconds = 0;
                minutes = 0;
                milliSeconds = 0;
                textView.setText(R.string.stopwatch_tv);

                listElementsArrayList.clear();
                adapter.notifyDataSetChanged();
                btnSave.setVisibility(View.INVISIBLE);
                btnSave.setEnabled(false);
            }
        });

        btnLap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listElementsArrayList.add("Отрезок " + (listElementsArrayList.size()+1) + ": " + textView.getText().toString());
                adapter.notifyDataSetChanged();
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mAuth = FirebaseAuth.getInstance();
                firebaseUser = mAuth.getCurrentUser();
                if (firebaseUser == null) {
                    NeedForAuthFragment needForAuthFragment = new NeedForAuthFragment();
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container, needForAuthFragment).commit();
                } else {
                    String userID = firebaseUser.getUid();
                    database = FirebaseDatabase.getInstance();
                    reference = database.getReference(userID);

                    showAlertDialog(timerView.getContext());
                }
            }
        });

        btnResult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ResultFragment resultFragment = new ResultFragment();
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container,resultFragment).commit();
                Toast.makeText(getContext(), R.string.menu_results, Toast.LENGTH_SHORT).show();
            }
        });

        return timerView;
    }

    public Runnable runnable = new Runnable() {

        public void run() {

            millisecondTime = SystemClock.uptimeMillis() - startTime;

            updateTime = timeBuff + millisecondTime;

            seconds = (int) (updateTime / 1000);

            minutes = seconds / 60;

            seconds = seconds % 60;

            milliSeconds = (int) (updateTime % 1000);

            textView.setText(minutes + ":"
                    + String.format("%02d", seconds) + ":"
                    + String.format("%03d", milliSeconds));

            handler.postDelayed(this, 0);
        }

    };

    private void showAlertDialog(Context context) {

        toggleSexString = getResources().getString(R.string.generalities4);
        togglePoolSizeString = getResources().getString(R.string.pool_size25);

        final AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_timer, null);
        dialog.setView(view);

        textViewDialog = view.findViewById(R.id.textview_dialog);
        textViewDialog.setText(textView.getText());

        toggleSex = view.findViewById(R.id.toggle_btn_dialog);
        toggleSex.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (toggleSex.isChecked()) {
                    toggleSexString = getResources().getString(R.string.generalities3);
                } else toggleSexString = getResources().getString(R.string.generalities4);
            }
        });

        textViewAge = view.findViewById(R.id.textview_age);
        ageSeekBar = view.findViewById(R.id.seekbar_age);
        //textViewAge.setText(String.valueOf(ageProgress));
        ageSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                int ageProgress = ageSeekBar.getProgress();
                textViewAge.setText(String.valueOf(ageProgress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        togglePoolSize = view.findViewById(R.id.toggle_pool_size);
        togglePoolSize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (togglePoolSize.isChecked()) {
                    togglePoolSizeString = getResources().getString(R.string.pool_size50);
                } else togglePoolSizeString = getResources().getString(R.string.pool_size25);
            }
        });

        ArrayAdapter<CharSequence> adapterType = ArrayAdapter.createFromResource(context,
                R.array.spinnerType, android.R.layout.simple_spinner_dropdown_item);
        adapterType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerType = view.findViewById(R.id.spinner_type);
        spinnerType.setAdapter(adapterType);
        spinnerType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                spinnerTypeString = String.valueOf(parent.getItemAtPosition(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter<CharSequence> adapterDist = ArrayAdapter.createFromResource(context,
                R.array.spinnerDist, android.R.layout.simple_spinner_dropdown_item);
        adapterDist.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerDist = view.findViewById(R.id.spinner_dist);
        spinnerDist.setAdapter(adapterDist);
        spinnerDist.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                spinnerDistString = String.valueOf(parent.getItemAtPosition(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        dialog.setPositiveButton(R.string.dialogSave, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                    addResults();
                    Toast.makeText(getContext(), toggleSexString + ", " +
                            textViewAge.getText() + " лет, " + spinnerTypeString + " (" +
                            togglePoolSizeString + "), " + spinnerDistString + ", " +
                            textView.getText() + "\n\n\n" + getString(R.string.dialogIsSaved), Toast.LENGTH_LONG).show();
            }
        });

        dialog.setNegativeButton(R.string.dialogCancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getContext(), R.string.dialogCancel, Toast.LENGTH_SHORT).show();
            }
        });

        dialog.show();
    }

    private void addResults() {
        String id = reference.push().getKey();
        ResultModel newResult = new ResultModel(toggleSexString, String.valueOf(textView.getText()),
                spinnerDistString, String.valueOf(textViewAge.getText()),
                id, spinnerTypeString + " (" + togglePoolSizeString + ")");

        Map<String, Object> resultValues = newResult.toMap();
        Map<String, Object> results = new HashMap<>();
        results.put(id, resultValues);

        reference.updateChildren(results);
    }
}
