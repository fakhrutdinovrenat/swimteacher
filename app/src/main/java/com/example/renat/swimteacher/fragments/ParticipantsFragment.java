package com.example.renat.swimteacher.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.transition.Slide;
import android.transition.TransitionManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.renat.swimteacher.R;

public class ParticipantsFragment extends Fragment {

    private Spinner spinnerParticipants;
    private LinearLayout llContainer, llPart1, llPart2, llPart3, llPart4, llPart5;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View participantsView = inflater.inflate(R.layout.fragment_participants, container, false);

        llContainer = participantsView.findViewById(R.id.participants_container);
        llPart1 = participantsView.findViewById(R.id.llpart1);
        llPart2 = participantsView.findViewById(R.id.llpart2);
        llPart3 = participantsView.findViewById(R.id.llpart3);
        llPart4 = participantsView.findViewById(R.id.llpart4);
        llPart5 = participantsView.findViewById(R.id.llpart5);

        ArrayAdapter<CharSequence> adapterParticipants = ArrayAdapter.createFromResource(getContext(),
                R.array.spinnerPartisipants, android.R.layout.simple_spinner_dropdown_item);
        adapterParticipants.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerParticipants = participantsView.findViewById(R.id.spinner_participants);
        spinnerParticipants.setAdapter(adapterParticipants);
        spinnerParticipants.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        TransitionManager.beginDelayedTransition(llContainer, new Slide(Gravity.START));
                        llPart1.setVisibility(View.VISIBLE);
                        llPart2.setVisibility(View.GONE);
                        llPart3.setVisibility(View.GONE);
                        llPart4.setVisibility(View.GONE);
                        llPart5.setVisibility(View.GONE);
                        break;
                    case 1:
                        TransitionManager.beginDelayedTransition(llContainer, new Slide(Gravity.START));
                        llPart1.setVisibility(View.GONE);
                        llPart2.setVisibility(View.VISIBLE);
                        llPart3.setVisibility(View.GONE);
                        llPart4.setVisibility(View.GONE);
                        llPart5.setVisibility(View.GONE);
                        break;
                    case 2:
                        TransitionManager.beginDelayedTransition(llContainer, new Slide(Gravity.START));
                        llPart1.setVisibility(View.GONE);
                        llPart2.setVisibility(View.GONE);
                        llPart3.setVisibility(View.VISIBLE);
                        llPart4.setVisibility(View.GONE);
                        llPart5.setVisibility(View.GONE);
                        break;
                    case 3:
                        TransitionManager.beginDelayedTransition(llContainer, new Slide(Gravity.START));
                        llPart1.setVisibility(View.GONE);
                        llPart2.setVisibility(View.GONE);
                        llPart3.setVisibility(View.GONE);
                        llPart4.setVisibility(View.VISIBLE);
                        llPart5.setVisibility(View.GONE);
                        break;
                    case 4:
                        TransitionManager.beginDelayedTransition(llContainer, new Slide(Gravity.START));
                        llPart1.setVisibility(View.GONE);
                        llPart2.setVisibility(View.GONE);
                        llPart3.setVisibility(View.GONE);
                        llPart4.setVisibility(View.GONE);
                        llPart5.setVisibility(View.VISIBLE);
                        break;
                }
                Toast.makeText(getActivity(), String.valueOf(parent.getItemAtPosition(position)), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        return participantsView;
    }
}
