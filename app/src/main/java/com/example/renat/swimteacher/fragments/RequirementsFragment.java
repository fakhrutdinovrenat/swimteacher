package com.example.renat.swimteacher.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.transition.Slide;
import android.transition.TransitionManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.renat.swimteacher.R;

public class RequirementsFragment extends Fragment {

    private Spinner spinnerRequirements;
    private LinearLayout llContainer, llReq1, llReq2, llReq3;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View requirementsView = inflater.inflate(R.layout.fragment_requieremets, container, false);

        llContainer = requirementsView.findViewById(R.id.requirements_container);
        llReq1 = requirementsView.findViewById(R.id.llreq1);
        llReq2 = requirementsView.findViewById(R.id.llreq2);
        llReq3 = requirementsView.findViewById(R.id.llreq3);

        ArrayAdapter<CharSequence> adapterRequirements = ArrayAdapter.createFromResource(getContext(),
                R.array.spinnerRequirements, android.R.layout.simple_spinner_dropdown_item);
        adapterRequirements.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerRequirements = requirementsView.findViewById(R.id.spinner_requirements);
        spinnerRequirements.setAdapter(adapterRequirements);
        spinnerRequirements.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        TransitionManager.beginDelayedTransition(llContainer, new Slide(Gravity.START));
                        llReq1.setVisibility(View.VISIBLE);
                        llReq2.setVisibility(View.GONE);
                        llReq3.setVisibility(View.GONE);
                        break;
                    case 1:
                        TransitionManager.beginDelayedTransition(llContainer, new Slide(Gravity.START));
                        llReq1.setVisibility(View.GONE);
                        llReq2.setVisibility(View.VISIBLE);
                        llReq3.setVisibility(View.GONE);
                        break;
                    case 2:
                        TransitionManager.beginDelayedTransition(llContainer, new Slide(Gravity.START));
                        llReq1.setVisibility(View.GONE);
                        llReq2.setVisibility(View.GONE);
                        llReq3.setVisibility(View.VISIBLE);
                        break;
                }

                Toast.makeText(getActivity(), String.valueOf(parent.getItemAtPosition(position)), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        return requirementsView;
    }
}
