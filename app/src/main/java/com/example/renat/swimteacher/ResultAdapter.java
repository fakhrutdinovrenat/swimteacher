package com.example.renat.swimteacher;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class ResultAdapter extends RecyclerView.Adapter<ResultAdapter.ResultViewHolder>
        implements Filterable {

    private Context context;
    private List<ResultModel> list;
    private List<ResultModel> filteredList;

    public ResultAdapter(Context context, List<ResultModel> list) {
        this.context = context;
        this.list = list;
        this.filteredList = list;
    }

    @Override
    public ResultViewHolder onCreateViewHolder  (ViewGroup parent, int viewType) {
        return new ResultViewHolder(LayoutInflater.from(parent.getContext()).
                inflate(R.layout.result_view_item, parent, false));
    }

    @Override
    public void onBindViewHolder(final ResultViewHolder holder, int position) {

        ResultModel resultModel = filteredList.get(position);

        holder.textSex.setText(new StringBuilder().append("Пол: ").append(resultModel.getSwimSex()).toString());
        holder.textAge.setText(new StringBuilder().append("Возраст: ").append(resultModel.getSwimAge()).toString());
        holder.textStyle.setText(new StringBuilder().append("Тип плавания: ").append(resultModel.getSwimStyle()).toString());
        holder.textDist.setText(new StringBuilder().append("Дистанция: ").append(resultModel.getSwimDist()).toString());
        holder.textSwimTime.setText(new StringBuilder().append("Результат: ").append(resultModel.getSwimTime()).toString());

        holder.itemView.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            @Override
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                menu.add(holder.getAdapterPosition(), 0, 0, "Удалить");
            }
        });
    }

    @Override
    public int getItemCount() {
        return filteredList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String search = constraint.toString();
                if(search.isEmpty()) {
                    filteredList = list;
                } else {
                    List<ResultModel> resultList = new ArrayList<>();
                    for (ResultModel row: list) {
                        if (row.getSwimSex().toLowerCase().contains(search.toLowerCase()) ||
                                row.getSwimAge().toLowerCase().contains(search.toLowerCase()) ||
                                row.getSwimStyle().toLowerCase().contains(search.toLowerCase()) ||
                                row.getSwimDist().toLowerCase().contains(search.toLowerCase()) ||
                                row.getSwimTime().toLowerCase().contains(search.toLowerCase())) {
                            resultList.add(row);
                        }
                    }

                    filteredList = resultList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                filteredList = (ArrayList<ResultModel>)results.values;
                notifyDataSetChanged();
            }
        };
    }

    class ResultViewHolder extends RecyclerView.ViewHolder{

        TextView textSex, textAge, textDist, textStyle, textSwimTime;

        public ResultViewHolder(View itemView) {
            super(itemView);

            textSex = itemView.findViewById(R.id.text_sex);
            textAge = itemView.findViewById(R.id.text_age);
            textDist = itemView.findViewById(R.id.text_dist);
            textStyle = itemView.findViewById(R.id.text_swimstyle);
            textSwimTime = itemView.findViewById(R.id.text_swimtime);
        }
    }
}