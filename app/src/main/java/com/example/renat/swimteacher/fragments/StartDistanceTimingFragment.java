package com.example.renat.swimteacher.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.transition.Slide;
import android.transition.TransitionManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.renat.swimteacher.R;

public class StartDistanceTimingFragment extends Fragment {

    private Spinner spinnerSDT;
    private LinearLayout llContainer, llsdt1, llsdt2, llsdt3;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View sdtView = inflater.inflate(R.layout.fragment_sdt, container, false);

        llContainer = sdtView.findViewById(R.id.sdt_container);
        llsdt1 = sdtView.findViewById(R.id.llsdt1);
        llsdt2 = sdtView.findViewById(R.id.llsdt2);
        llsdt3 = sdtView.findViewById(R.id.llsdt3);

        ArrayAdapter<CharSequence> adapterSDT = ArrayAdapter.createFromResource(getContext(),
                R.array.spinnerSDT, android.R.layout.simple_spinner_dropdown_item);
        adapterSDT.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerSDT = sdtView.findViewById(R.id.spinner_sdt);
        spinnerSDT.setAdapter(adapterSDT);
        spinnerSDT.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        TransitionManager.beginDelayedTransition(llContainer, new Slide(Gravity.START));
                        llsdt1.setVisibility(View.VISIBLE);
                        llsdt2.setVisibility(View.GONE);
                        llsdt3.setVisibility(View.GONE);
                        break;
                    case 1:
                        TransitionManager.beginDelayedTransition(llContainer, new Slide(Gravity.START));
                        llsdt1.setVisibility(View.GONE);
                        llsdt2.setVisibility(View.VISIBLE);
                        llsdt3.setVisibility(View.GONE);
                        break;
                    case 2:
                        TransitionManager.beginDelayedTransition(llContainer, new Slide(Gravity.START));
                        llsdt1.setVisibility(View.GONE);
                        llsdt2.setVisibility(View.GONE);
                        llsdt3.setVisibility(View.VISIBLE);
                        break;
                }

                Toast.makeText(getActivity(), String.valueOf(parent.getItemAtPosition(position)), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        return sdtView;
    }
}
