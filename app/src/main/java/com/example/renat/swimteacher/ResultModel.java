package com.example.renat.swimteacher;

import java.util.HashMap;
import java.util.Map;

public class ResultModel {
    public String swimSex, swimTime, swimDist, key, swimStyle, swimAge;

    public ResultModel() {
    }

    public ResultModel(String swimSex, String swimTime, String swimDist,
                       String swimAge, String key, String swimStyle) {
        this.swimSex = swimSex;
        this.swimTime = swimTime;
        this.swimDist = swimDist;
        this.swimAge = swimAge;
        this.key = key;
        this.swimStyle = swimStyle;
    }

    public String getSwimSex() {
        return swimSex;
    }

    public void setSwimSex(String swimSex) {
        this.swimSex = swimSex;
    }

    public String getSwimTime() {
        return swimTime;
    }

    public void setSwimTime(String swimTime) {
        this.swimTime = swimTime;
    }

    public String getSwimDist() {
        return swimDist;
    }

    public void setSwimDist(String swimDist) {
        this.swimDist = swimDist;
    }

    public String getSwimStyle() {
        return swimStyle;
    }

    public void setSwimStyle(String swimStyle) {
        this.swimStyle = swimStyle;
    }

    public String getSwimAge() {
        return swimAge;
    }

    public void setSwimAge(String swimAge) {
        this.swimAge = swimAge;
    }

    public Map<String, Object> toMap() {
        HashMap<String,Object> result = new HashMap<>();
        result.put("swimSex", swimSex);
        result.put("swimTime", swimTime);
        result.put("swimDist", swimDist);
        result.put("swimAge", swimAge);
        result.put("key", key);
        result.put("swimStyle", swimStyle);

        return result;

    }
}