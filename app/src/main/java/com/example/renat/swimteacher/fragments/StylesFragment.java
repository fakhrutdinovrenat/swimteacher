package com.example.renat.swimteacher.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.transition.Slide;
import android.transition.TransitionManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.renat.swimteacher.R;

public class StylesFragment extends Fragment {

    private Spinner spinnerStyles;
    private LinearLayout llContainer, llStyles1, llStyles2, llStyles3, llStyles4, llStyles5;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View styleView = inflater.inflate(R.layout.fragment_styles, container, false);

        llContainer = styleView.findViewById(R.id.styles_container);
        llStyles1 = styleView.findViewById(R.id.llstyle1);
        llStyles2 = styleView.findViewById(R.id.llstyle2);
        llStyles3 = styleView.findViewById(R.id.llstyle3);
        llStyles4 = styleView.findViewById(R.id.llstyle4);
        llStyles5 = styleView.findViewById(R.id.llstyle5);

        ArrayAdapter<CharSequence> adapterStyles = ArrayAdapter.createFromResource(getContext(),
                R.array.spinnerStyles, android.R.layout.simple_spinner_dropdown_item);
        adapterStyles.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerStyles = styleView.findViewById(R.id.spinner_styles);
        spinnerStyles.setAdapter(adapterStyles);
        spinnerStyles.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        TransitionManager.beginDelayedTransition(llContainer, new Slide(Gravity.START));
                        llStyles1.setVisibility(View.VISIBLE);
                        llStyles2.setVisibility(View.GONE);
                        llStyles3.setVisibility(View.GONE);
                        llStyles4.setVisibility(View.GONE);
                        llStyles5.setVisibility(View.GONE);
                        break;
                    case 1:
                        TransitionManager.beginDelayedTransition(llContainer, new Slide(Gravity.START));
                        llStyles1.setVisibility(View.GONE);
                        llStyles2.setVisibility(View.VISIBLE);
                        llStyles3.setVisibility(View.GONE);
                        llStyles4.setVisibility(View.GONE);
                        llStyles5.setVisibility(View.GONE);
                        break;
                    case 2:
                        TransitionManager.beginDelayedTransition(llContainer, new Slide(Gravity.START));
                        llStyles1.setVisibility(View.GONE);
                        llStyles2.setVisibility(View.GONE);
                        llStyles3.setVisibility(View.VISIBLE);
                        llStyles4.setVisibility(View.GONE);
                        llStyles5.setVisibility(View.GONE);
                        break;
                    case 3:
                        TransitionManager.beginDelayedTransition(llContainer, new Slide(Gravity.START));
                        llStyles1.setVisibility(View.GONE);
                        llStyles2.setVisibility(View.GONE);
                        llStyles3.setVisibility(View.GONE);
                        llStyles4.setVisibility(View.VISIBLE);
                        llStyles5.setVisibility(View.GONE);
                        break;
                    case 4:
                        TransitionManager.beginDelayedTransition(llContainer, new Slide(Gravity.START));
                        llStyles1.setVisibility(View.GONE);
                        llStyles2.setVisibility(View.GONE);
                        llStyles3.setVisibility(View.GONE);
                        llStyles4.setVisibility(View.GONE);
                        llStyles5.setVisibility(View.VISIBLE);
                        break;
                }

                Toast.makeText(getActivity(), String.valueOf(parent.getItemAtPosition(position)), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        return styleView;
    }
}
