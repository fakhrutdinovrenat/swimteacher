package com.example.renat.swimteacher.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.transition.Slide;
import android.transition.TransitionManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.renat.swimteacher.R;

public class JudgesFragment extends Fragment {

    private Spinner spinnerJudges;
    private LinearLayout llContainer, llJud1, llJud2, llJud3, llJud4, llJud5, llJud6;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View judgesView = inflater.inflate(R.layout.fragment_judges, container, false);

        llContainer = judgesView.findViewById(R.id.judges_container);
        llJud1 = judgesView.findViewById(R.id.lljud1);
        llJud2 = judgesView.findViewById(R.id.lljud2);
        llJud3 = judgesView.findViewById(R.id.lljud3);
        llJud4 = judgesView.findViewById(R.id.lljud4);
        llJud5 = judgesView.findViewById(R.id.lljud5);
        llJud6 = judgesView.findViewById(R.id.lljud6);

        ArrayAdapter<CharSequence> adapterJudges = ArrayAdapter.createFromResource(getContext(),
                R.array.spinnerJudges, android.R.layout.simple_spinner_dropdown_item);
        adapterJudges.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerJudges = judgesView.findViewById(R.id.spinner_judges);
        spinnerJudges.setAdapter(adapterJudges);
        spinnerJudges.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        TransitionManager.beginDelayedTransition(llContainer, new Slide(Gravity.START));
                        llJud1.setVisibility(View.VISIBLE);
                        llJud2.setVisibility(View.GONE);
                        llJud3.setVisibility(View.GONE);
                        llJud4.setVisibility(View.GONE);
                        llJud5.setVisibility(View.GONE);
                        llJud6.setVisibility(View.GONE);
                        break;
                    case 1:
                        TransitionManager.beginDelayedTransition(llContainer, new Slide(Gravity.START));
                        llJud1.setVisibility(View.GONE);
                        llJud2.setVisibility(View.VISIBLE);
                        llJud3.setVisibility(View.GONE);
                        llJud4.setVisibility(View.GONE);
                        llJud5.setVisibility(View.GONE);
                        llJud6.setVisibility(View.GONE);
                        break;
                    case 2:
                        TransitionManager.beginDelayedTransition(llContainer, new Slide(Gravity.START));
                        llJud1.setVisibility(View.GONE);
                        llJud2.setVisibility(View.GONE);
                        llJud3.setVisibility(View.VISIBLE);
                        llJud4.setVisibility(View.GONE);
                        llJud5.setVisibility(View.GONE);
                        llJud6.setVisibility(View.GONE);
                        break;
                    case 3:
                        TransitionManager.beginDelayedTransition(llContainer, new Slide(Gravity.START));
                        llJud1.setVisibility(View.GONE);
                        llJud2.setVisibility(View.GONE);
                        llJud3.setVisibility(View.GONE);
                        llJud4.setVisibility(View.VISIBLE);
                        llJud5.setVisibility(View.GONE);
                        llJud6.setVisibility(View.GONE);
                        break;
                    case 4:
                        TransitionManager.beginDelayedTransition(llContainer, new Slide(Gravity.START));
                        llJud1.setVisibility(View.GONE);
                        llJud2.setVisibility(View.GONE);
                        llJud3.setVisibility(View.GONE);
                        llJud4.setVisibility(View.GONE);
                        llJud5.setVisibility(View.VISIBLE);
                        llJud6.setVisibility(View.GONE);
                        break;
                    case 5:
                        TransitionManager.beginDelayedTransition(llContainer, new Slide(Gravity.START));
                        llJud1.setVisibility(View.GONE);
                        llJud2.setVisibility(View.GONE);
                        llJud3.setVisibility(View.GONE);
                        llJud4.setVisibility(View.GONE);
                        llJud5.setVisibility(View.GONE);
                        llJud6.setVisibility(View.VISIBLE);
                        break;
                }

                Toast.makeText(getActivity(), String.valueOf(parent.getItemAtPosition(position)), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        return judgesView;
    }
}
