package com.example.renat.swimteacher.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SearchView;
import android.widget.Toast;

import com.example.renat.swimteacher.R;
import com.example.renat.swimteacher.ResultAdapter;
import com.example.renat.swimteacher.ResultModel;
import com.example.renat.swimteacher.firebase.Auth;
import com.example.renat.swimteacher.firebase.Data;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class ResultFragment extends Fragment implements Auth, Data {

    Button btnTimer;
    RecyclerView recyclerView;
    List<ResultModel> result;
    ResultAdapter adapter;
    SearchView searchView;

    public ResultFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View resultView = inflater.inflate(R.layout.fragment_result, container, false);

        searchView = resultView.findViewById(R.id.search_result);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                adapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return false;
            }
        });

        result = new ArrayList<>();

        recyclerView = resultView.findViewById(R.id.result_list);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(resultView.getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);

        recyclerView.setLayoutManager(llm);

        adapter = new ResultAdapter(resultView.getContext(), result);
        recyclerView.setAdapter(adapter);

        btnTimer = resultView.findViewById(R.id.btnTimer);
        btnTimer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimerFragment timerFragment = new TimerFragment();
                Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction().replace(R.id.container, timerFragment).commit();
                Toast.makeText(getContext(), R.string.stopwatch_hello, Toast.LENGTH_SHORT).show();
            }
        });

        updateList();

        return resultView;
    }

    private void updateList() {
        reference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                result.add(dataSnapshot.getValue(ResultModel.class));
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
                ResultModel resultModel = dataSnapshot.getValue(ResultModel.class);

                int index = getItemIndex(resultModel);

                result.remove(index);
                adapter.notifyItemRemoved(index);
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private int getItemIndex(ResultModel resultModel) {
        int index = 0;

        for (int i = 0; i<result.size(); i++) {
            if (result.get(i).key.equals(resultModel.key)) {
                index = i;
                break;
            }
        }
        return index;
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 0:
                removeResult(item.getGroupId());
                Toast.makeText(getContext(), R.string.deleted, Toast.LENGTH_SHORT).show();
                break;
            //добавим что-нибудь, если возникнет потребность, прописав "case 1:"
        }

        return super.onContextItemSelected(item);
    }

    private void removeResult(int position) {
        reference.child(result.get(position).key).removeValue();
    }
}
