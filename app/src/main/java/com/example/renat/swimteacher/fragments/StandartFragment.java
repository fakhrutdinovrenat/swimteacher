package com.example.renat.swimteacher.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.transition.Slide;
import android.transition.TransitionManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.andexert.expandablelayout.library.ExpandableLayout;
import com.andexert.expandablelayout.library.ExpandableLayoutItem;
import com.andexert.expandablelayout.library.ExpandableLayoutListView;
import com.example.renat.swimteacher.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StandartFragment extends Fragment {

    //private Spinner spinnerStandart;
    //private LinearLayout standartLayout;
    /*private FrameLayout tableMSMK, tableMSKMS, tableNormMSMK, tableNormMS, tableNormKMS, tableNormIR,
            tableNormIIR, tableNormIIIR, tableNormIUR, tableNormIIUR, tableNormIIIUR;*/

    //создаем массив с идентификаторами ячеек таблицы нормативов
    /*private int[] list_standart = {
            R.id.table_tv_1, R.id.table_tv_2, R.id.table_tv_3, R.id.table_tv_4, R.id.table_tv_5,
            R.id.table_tv_6, R.id.table_tv_7, R.id.table_tv_8, R.id.table_tv_9, R.id.table_tv_10,
            R.id.table_tv_11, R.id.table_tv_12, R.id.table_tv_13, R.id.table_tv_14, R.id.table_tv_15,
            R.id.table_tv_16, R.id.table_tv_17, R.id.table_tv_18, R.id.table_tv_19, R.id.table_tv_20,
            R.id.table_tv_21, R.id.table_tv_22, R.id.table_tv_23, R.id.table_tv_24, R.id.table_tv_25,
            R.id.table_tv_26, R.id.table_tv_27, R.id.table_tv_28, R.id.table_tv_29, R.id.table_tv_30,
            R.id.table_tv_31, R.id.table_tv_32, R.id.table_tv_33, R.id.table_tv_34, R.id.table_tv_35,
            R.id.table_tv_36, R.id.table_tv_37, R.id.table_tv_38, R.id.table_tv_39, R.id.table_tv_40,
            R.id.table_tv_41, R.id.table_tv_42, R.id.table_tv_43, R.id.table_tv_44, R.id.table_tv_45,
            R.id.table_tv_46, R.id.table_tv_47, R.id.table_tv_48, R.id.table_tv_49, R.id.table_tv_50,
            R.id.table_tv_51, R.id.table_tv_52, R.id.table_tv_53, R.id.table_tv_54, R.id.table_tv_55,
            R.id.table_tv_56, R.id.table_tv_57, R.id.table_tv_58, R.id.table_tv_59, R.id.table_tv_60,
            R.id.table_tv_61, R.id.table_tv_62, R.id.table_tv_63, R.id.table_tv_64, R.id.table_tv_65,
            R.id.table_tv_66, R.id.table_tv_67, R.id.table_tv_68, R.id.table_tv_69, R.id.table_tv_70,
            R.id.table_tv_71, R.id.table_tv_72, R.id.table_tv_73, R.id.table_tv_74, R.id.table_tv_75,
            R.id.table_tv_76, R.id.table_tv_77, R.id.table_tv_78, R.id.table_tv_79, R.id.table_tv_80,
            R.id.table_tv_81, R.id.table_tv_82, R.id.table_tv_83, R.id.table_tv_84, R.id.table_tv_85,
            R.id.table_tv_86,
    };*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View standartView = inflater.inflate(R.layout.fragment_standart, container, false);

        //standartLayout = standartView.findViewById(R.id.standart_container);


        /**
         ExpandableListView из библиотеки. С помощью массивов заполняем данные в таблицах и items

        final String[] list_tables = standartView.getResources().getStringArray(R.array.spinnerStandart);
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getContext(), R.layout.view_row, R.id.text_header, list_tables);
        final ExpandableLayoutListView expandableLayoutListView = standartView.findViewById(R.id.expandableList_tables);
        expandableLayoutListView.setAdapter(arrayAdapter);

         **/

        /*Создаем массивы с временными нормативами по плаванию, чтобы потом вывести в TextViews в таблицах
        final String[] list_norm_mcmk = standartView.getResources().getStringArray(R.array.table_MCMK);
        final String[] list_norm_mc = standartView.getResources().getStringArray(R.array.table_MC);
        final String[] list_norm_kmk = standartView.getResources().getStringArray(R.array.table_KMC);
        final String[] list_norm_first_r = standartView.getResources().getStringArray(R.array.table_first_r);
        final String[] list_norm_second_r = standartView.getResources().getStringArray(R.array.table_second_r);
        final String[] list_norm_third_r = standartView.getResources().getStringArray(R.array.table_third_r);
        final String[] list_norm_first_ur = standartView.getResources().getStringArray(R.array.table_first_ur);
        final String[] list_norm_second_ur = standartView.getResources().getStringArray(R.array.table_second_ur);
        final String[] list_norm_third_ur = standartView.getResources().getStringArray(R.array.table_third_ur);
        */

        /*tableMSMK = standartView.findViewById(R.id.msmkItem);
        tableMSKMS = standartView.findViewById(R.id.mskmsItem);
        tableNormMSMK = standartView.findViewById(R.id.norm_msmk_item);

        ArrayAdapter<CharSequence> adapterStandart = ArrayAdapter.createFromResource(getContext(),
                R.array.spinnerStandart, android.R.layout.simple_spinner_dropdown_item);
        adapterStandart.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerStandart = standartView.findViewById(R.id.spinner_standarts);
        spinnerStandart.setAdapter(adapterStandart);
        spinnerStandart.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                switch (position) {
                    case 0:
                        TransitionManager.beginDelayedTransition(standartLayout, new Slide(Gravity.START));
                        tableMSMK.setVisibility(View.VISIBLE);
                        tableMSKMS.setVisibility(View.GONE);
                        tableNormMSMK.setVisibility(View.GONE);
                        break;
                    case 1:
                        TransitionManager.beginDelayedTransition(standartLayout, new Slide(Gravity.START));
                        tableMSMK.setVisibility(View.GONE);
                        tableMSKMS.setVisibility(View.VISIBLE);
                        tableNormMSMK.setVisibility(View.GONE);
                        break;
                    case 2:
                        TransitionManager.beginDelayedTransition(standartLayout, new Slide(Gravity.START));
                        tableMSMK.setVisibility(View.GONE);
                        tableMSKMS.setVisibility(View.GONE);
                        tableNormMSMK.setVisibility(View.VISIBLE);

                        for (int i = 0; i<list_standart.length; i++) {
                            TextView tv = standartView.findViewById(list_standart[i]);
                            tv.setText(list_norm_mcmk[i]);
                        }
                        break;

                    case 3:
                        TransitionManager.beginDelayedTransition(standartLayout, new Slide(Gravity.START));
                        tableMSMK.setVisibility(View.GONE);
                        tableMSKMS.setVisibility(View.GONE);
                        tableNormMSMK.setVisibility(View.VISIBLE);

                        for (int i = 0; i<list_standart.length; i++) {
                            TextView tv = standartView.findViewById(list_standart[i]);
                            tv.setText(list_norm_mc[i]);
                        }
                        break;
                    case 4:
                        TransitionManager.beginDelayedTransition(standartLayout, new Slide(Gravity.START));
                        tableMSMK.setVisibility(View.GONE);
                        tableMSKMS.setVisibility(View.GONE);
                        tableNormMSMK.setVisibility(View.VISIBLE);

                        for (int i = 0; i<list_standart.length; i++) {
                            TextView tv = standartView.findViewById(list_standart[i]);
                            tv.setText(list_norm_kmk[i]);
                        }
                        break;
                    case 5:
                        TransitionManager.beginDelayedTransition(standartLayout, new Slide(Gravity.START));
                        tableMSMK.setVisibility(View.GONE);
                        tableMSKMS.setVisibility(View.GONE);
                        tableNormMSMK.setVisibility(View.VISIBLE);

                        for (int i = 0; i<list_standart.length; i++) {
                            TextView tv = standartView.findViewById(list_standart[i]);
                            tv.setText(list_norm_first_r[i]);
                        }
                        break;
                    case 6:
                        TransitionManager.beginDelayedTransition(standartLayout, new Slide(Gravity.START));
                        tableMSMK.setVisibility(View.GONE);
                        tableMSKMS.setVisibility(View.GONE);
                        tableNormMSMK.setVisibility(View.VISIBLE);

                        for (int i = 0; i<list_standart.length; i++) {
                            TextView tv = standartView.findViewById(list_standart[i]);
                            tv.setText(list_norm_second_r[i]);
                        }
                        break;
                    case 7:
                        TransitionManager.beginDelayedTransition(standartLayout, new Slide(Gravity.START));
                        tableMSMK.setVisibility(View.GONE);
                        tableMSKMS.setVisibility(View.GONE);
                        tableNormMSMK.setVisibility(View.VISIBLE);

                        for (int i = 0; i<list_standart.length; i++) {
                            TextView tv = standartView.findViewById(list_standart[i]);
                            tv.setText(list_norm_third_r[i]);
                        }
                        break;
                    case 8:
                        TransitionManager.beginDelayedTransition(standartLayout, new Slide(Gravity.START));
                        tableMSMK.setVisibility(View.GONE);
                        tableMSKMS.setVisibility(View.GONE);
                        tableNormMSMK.setVisibility(View.VISIBLE);

                        for (int i = 0; i<list_standart.length; i++) {
                            TextView tv = standartView.findViewById(list_standart[i]);
                            tv.setText(list_norm_first_ur[i]);
                        }
                        break;
                    case 9:
                        TransitionManager.beginDelayedTransition(standartLayout, new Slide(Gravity.START));
                        tableMSMK.setVisibility(View.GONE);
                        tableMSKMS.setVisibility(View.GONE);
                        tableNormMSMK.setVisibility(View.VISIBLE);

                        for (int i = 0; i<list_standart.length; i++) {
                            TextView tv = standartView.findViewById(list_standart[i]);
                            tv.setText(list_norm_second_ur[i]);
                        }
                        break;
                    case 10:
                        TransitionManager.beginDelayedTransition(standartLayout, new Slide(Gravity.START));
                        tableMSMK.setVisibility(View.GONE);
                        tableMSKMS.setVisibility(View.GONE);
                        tableNormMSMK.setVisibility(View.VISIBLE);

                        for (int i = 0; i<list_standart.length; i++) {
                            TextView tv = standartView.findViewById(list_standart[i]);
                            tv.setText(list_norm_third_ur[i]);
                        }
                        break;
                }

                Toast.makeText(getActivity(), String.valueOf(parent.getItemAtPosition(position)), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        */
        return standartView;
    }
}
