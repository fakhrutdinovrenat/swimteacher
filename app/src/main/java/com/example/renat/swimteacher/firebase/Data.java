package com.example.renat.swimteacher.firebase;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import static com.example.renat.swimteacher.firebase.Auth.firebaseUser;

public interface Data {
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference reference = database.getReference(firebaseUser.getUid());
}
