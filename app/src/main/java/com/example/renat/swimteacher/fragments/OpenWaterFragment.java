package com.example.renat.swimteacher.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.transition.Slide;
import android.transition.TransitionManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.renat.swimteacher.R;

public class OpenWaterFragment extends Fragment {

    private Spinner spinnerOpenWater;
    private LinearLayout llContainer, llOpenWater1, llOpenWater2, llOpenWater3, llOpenWater4, llOpenWater5;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View openwaterView = inflater.inflate(R.layout.fragment_openwater, container, false);

        llContainer = openwaterView.findViewById(R.id.openwater_container);
        llOpenWater1 = openwaterView.findViewById(R.id.llopenwater1);
        llOpenWater2 = openwaterView.findViewById(R.id.llopenwater2);
        llOpenWater3 = openwaterView.findViewById(R.id.llopenwater3);
        llOpenWater4 = openwaterView.findViewById(R.id.llopenwater4);
        llOpenWater5 = openwaterView.findViewById(R.id.llopenwater5);

        ArrayAdapter<CharSequence> adapterOpenWater = ArrayAdapter.createFromResource(getContext(),
                R.array.spinnerOpenWater, android.R.layout.simple_spinner_dropdown_item);
        adapterOpenWater.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerOpenWater = openwaterView.findViewById(R.id.spinner_openwater);
        spinnerOpenWater.setAdapter(adapterOpenWater);
        spinnerOpenWater.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        TransitionManager.beginDelayedTransition(llContainer, new Slide(Gravity.START));
                        llOpenWater1.setVisibility(View.VISIBLE);
                        llOpenWater2.setVisibility(View.GONE);
                        llOpenWater3.setVisibility(View.GONE);
                        llOpenWater4.setVisibility(View.GONE);
                        llOpenWater5.setVisibility(View.GONE);
                        break;
                    case 1:
                        TransitionManager.beginDelayedTransition(llContainer, new Slide(Gravity.START));
                        llOpenWater1.setVisibility(View.GONE);
                        llOpenWater2.setVisibility(View.VISIBLE);
                        llOpenWater3.setVisibility(View.GONE);
                        llOpenWater4.setVisibility(View.GONE);
                        llOpenWater5.setVisibility(View.GONE);
                        break;
                    case 2:
                        TransitionManager.beginDelayedTransition(llContainer, new Slide(Gravity.START));
                        llOpenWater1.setVisibility(View.GONE);
                        llOpenWater2.setVisibility(View.GONE);
                        llOpenWater3.setVisibility(View.VISIBLE);
                        llOpenWater4.setVisibility(View.GONE);
                        llOpenWater5.setVisibility(View.GONE);
                        break;
                    case 3:
                        TransitionManager.beginDelayedTransition(llContainer, new Slide(Gravity.START));
                        llOpenWater1.setVisibility(View.GONE);
                        llOpenWater2.setVisibility(View.GONE);
                        llOpenWater3.setVisibility(View.GONE);
                        llOpenWater4.setVisibility(View.VISIBLE);
                        llOpenWater5.setVisibility(View.GONE);
                        break;
                    case 4:
                        TransitionManager.beginDelayedTransition(llContainer, new Slide(Gravity.START));
                        llOpenWater1.setVisibility(View.GONE);
                        llOpenWater2.setVisibility(View.GONE);
                        llOpenWater3.setVisibility(View.GONE);
                        llOpenWater4.setVisibility(View.GONE);
                        llOpenWater5.setVisibility(View.VISIBLE);
                        break;
                }

                Toast.makeText(getActivity(), String.valueOf(parent.getItemAtPosition(position)), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        return openwaterView;
    }
}
