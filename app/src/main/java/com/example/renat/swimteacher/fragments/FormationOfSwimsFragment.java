package com.example.renat.swimteacher.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.transition.Slide;
import android.transition.TransitionManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;

import com.example.renat.swimteacher.R;
import com.example.renat.swimteacher.presenters.FormationOfSwimsPresenter;
import com.example.renat.swimteacher.views.FormationOfSwimsView;
import com.ms.square.android.expandabletextview.ExpandableTextView;

import java.util.Objects;

public class FormationOfSwimsFragment extends MvpAppCompatFragment implements FormationOfSwimsView {

    @InjectPresenter
    FormationOfSwimsPresenter mFormationOfSwimsPresenter;

    private View formView;
    private ExpandableTextView expandableTextView;
    private LinearLayout llContainer, llForm1, llForm2;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        formView = inflater.inflate(R.layout.fragment_formationofswims, container, false);

        expandableTextView = formView.findViewById(R.id.expand_text_view);

        llContainer = formView.findViewById(R.id.formation_container);
        llForm1 = formView.findViewById(R.id.llfor1);
        llForm2 = formView.findViewById(R.id.llfor2);

        createSpinner();
        return formView;
    }

    @Override
    public void createSpinner() {
        ArrayAdapter<CharSequence> adapterFormation = ArrayAdapter.createFromResource(Objects.requireNonNull(getContext()),
                R.array.spinnerFormation, android.R.layout.simple_spinner_dropdown_item);
        adapterFormation.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Spinner spinnerForm = formView.findViewById(R.id.spinner_formation);
        spinnerForm.setAdapter(adapterFormation);
        spinnerForm.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        slideAnimation();
                        llForm1.setVisibility(View.VISIBLE);
                        llForm2.setVisibility(View.GONE);
                        break;
                    case 1:
                        slideAnimation();
                        llForm1.setVisibility(View.GONE);
                        llForm2.setVisibility(View.VISIBLE);
                        break;
                }

                /* Убран Toast, так как он дублирует информацию. В этом нет необходимости.
                Toast.makeText(getContext(), String.valueOf(parent.getItemAtPosition(position)), Toast.LENGTH_SHORT).show();
                 */
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void setMessage(int message) {
        expandableTextView.setText(getString(message));
    }

    @Override
    public void slideAnimation() {
        TransitionManager.beginDelayedTransition(llContainer, new Slide(Gravity.START));
    }
}
