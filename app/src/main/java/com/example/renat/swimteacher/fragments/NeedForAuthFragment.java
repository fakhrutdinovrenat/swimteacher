package com.example.renat.swimteacher.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.renat.swimteacher.GoogleSignInActivity;
import com.example.renat.swimteacher.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class NeedForAuthFragment extends Fragment {

    Button button;

    public NeedForAuthFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_need_for_auth, container, false);

        button = view.findViewById(R.id.btnToAuth);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), GoogleSignInActivity.class));
            }
        });
        return view;
    }

}
