package com.example.renat.swimteacher.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.renat.swimteacher.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProtestFormFragment extends Fragment {

    private ImageView protestImage;


    public ProtestFormFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View protestformView = inflater.inflate(R.layout.fragment_protest_form, container, false);

        return protestformView;
    }

}
