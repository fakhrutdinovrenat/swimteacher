package com.example.renat.swimteacher;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.renat.swimteacher.firebase.Auth;
import com.example.renat.swimteacher.fragments.FormationOfSwimsFragment;
import com.example.renat.swimteacher.fragments.GeneralitiesFragment;
import com.example.renat.swimteacher.fragments.JudgesFragment;
import com.example.renat.swimteacher.fragments.ManagementFragment;
import com.example.renat.swimteacher.fragments.NeedForAuthFragment;
import com.example.renat.swimteacher.fragments.OpenWaterFragment;
import com.example.renat.swimteacher.fragments.ParticipantsFragment;
import com.example.renat.swimteacher.fragments.ProtestFragment;
import com.example.renat.swimteacher.fragments.RequirementsFragment;
import com.example.renat.swimteacher.fragments.ResultFragment;
import com.example.renat.swimteacher.fragments.StandartFragment;
import com.example.renat.swimteacher.fragments.StartDistanceTimingFragment;
import com.example.renat.swimteacher.fragments.StylesFragment;
import com.example.renat.swimteacher.fragments.TimerFragment;

import com.example.renat.swimteacher.presenters.MainPresentation;
import com.example.renat.swimteacher.views.MainView;

public class MainActivity extends MvpAppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, MainView, Auth {

    @InjectPresenter
    MainPresentation mMainPresentation;

    private DrawerLayout drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);


        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.nav_standarts) {
            StandartFragment standartFragment = new StandartFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.container, standartFragment, standartFragment.getTag()).commit();
        } else if (id == R.id.nav_generalities) {
            GeneralitiesFragment generalitiesFragment = new GeneralitiesFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.container, generalitiesFragment, generalitiesFragment.getTag()).commit();
        } else if (id == R.id.nav_participants) {
            ParticipantsFragment participantsFragment = new ParticipantsFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.container, participantsFragment, participantsFragment.getTag()).commit();
        } else if (id == R.id.nav_management) {
            ManagementFragment managementFragment = new ManagementFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.container, managementFragment, managementFragment.getTag()).commit();
        } else if (id == R.id.nav_judges) {
            JudgesFragment judgesFragment = new JudgesFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.container, judgesFragment, judgesFragment.getTag()).commit();
        } else if (id == R.id.nav_formation_swims) {
            FormationOfSwimsFragment formationOfSwimsFragment = new FormationOfSwimsFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.container, formationOfSwimsFragment, formationOfSwimsFragment.getTag()).commit();
        } else if (id == R.id.nav_SDT) {
            StartDistanceTimingFragment startDistanceTimingFragment = new StartDistanceTimingFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.container, startDistanceTimingFragment, startDistanceTimingFragment.getTag()).commit();
        } else if (id == R.id.nav_styles) {
            StylesFragment stylesFragment = new StylesFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.container, stylesFragment, stylesFragment.getTag()).commit();
        } else if (id == R.id.nav_protest) {
            ProtestFragment protestFragment = new ProtestFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.container, protestFragment, protestFragment.getTag()).commit();
        } else if (id == R.id.nav_requirements) {
            RequirementsFragment requirementsFragment = new RequirementsFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.container, requirementsFragment, requirementsFragment.getTag()).commit();
        } else if (id == R.id.nav_openwater) {
            OpenWaterFragment openWaterFragment = new OpenWaterFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.container, openWaterFragment, openWaterFragment.getTag()).commit();
        } else if (id == R.id.nav_timer) {
            TimerFragment timerFragment = new TimerFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.container, timerFragment, timerFragment.getTag()).commit();
            Toast.makeText(this, R.string.stopwatch_hello, Toast.LENGTH_SHORT).show();
        } else if (id == R.id.nav_results) {
            if (firebaseUser == null) {
                NeedForAuthFragment needForAuthFragment = new NeedForAuthFragment();
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.container, needForAuthFragment, needForAuthFragment.getTag()).commit();
            } else {
                ResultFragment resultFragment = new ResultFragment();
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.container, resultFragment, resultFragment.getTag()).commit();
                Toast.makeText(this, R.string.menu_results, Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_auth) {
            startActivity(new Intent(this, GoogleSignInActivity.class));
            Toast.makeText(this, R.string.authentication, Toast.LENGTH_SHORT).show();
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void onClickToLearn(View view) {
        drawer.openDrawer(GravityCompat.START);
    }

}

/*
Логику, которую следует внести
 1. открытие шторки drawer (метод скрытия и раскрытия шторки)
 2. открытие фрагментов (метод открытия фрагмента)
 3. Показ тоста
 4. Firebase

 */
