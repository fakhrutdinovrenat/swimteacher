package com.example.renat.swimteacher.presenters;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import com.example.renat.swimteacher.R;
import com.example.renat.swimteacher.views.FormationOfSwimsView;


@InjectViewState
public class FormationOfSwimsPresenter extends MvpPresenter<FormationOfSwimsView> {

    public FormationOfSwimsPresenter() {
        getViewState().setMessage(R.string.formation01);
        getViewState().slideAnimation();
        getViewState().createSpinner();
    }
}
